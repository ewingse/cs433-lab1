<?php
require 'User.php';
require 'UserDAO.php';

class UserTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @dataProvider userProvider
	 */
	function testAuthenticate($username, $password, $success)
	{		
		$user = new User();
		$user->username = 'daniel';
		$user->hash = '$2a$10$somesaltafawefwfesfwrenxnlWzt/b1kVHiQ2CWEgsVnY9qHDA42';
		
		$dao = $this->getMock('UserDAO');
		$dao->expects($this->any())
		    ->method('read')
		    ->will($this->returnValue($user));
		
		$authUser = new User($dao);
		$result = $authUser->authenticate($username, $password);
		$this->assertEquals($result, $success);
	}
	 
	function userProvider()
	{
		return array(
			array('daniel', 'password1', true),
			array('daniel', 'password', false),
			array('daniel', '', false),
			array('', 'password1', false),
			array('jimmy', 'fdsljfsld', false)
		);
	}
}

?>