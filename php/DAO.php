<?php

interface iDAO
{
	public function create($obj);
	public function read($id);
	public function update($obj);
	public function remove($id);
}

?>