<?php

//define('BCRYPT_COST', 10);
//define('PASSWORD_SALT', 'putsomesillystringhere');

class User
{
	public $username;
	public $firstname;
	public $lastname;
	public $hash;
	private $dao;
	
	# Dependency Injection
	public function __construct($dao = null)
	{
		$this->dao = ($dao ? $dao : new UserDAO());
	}
	
	public function authenticate($username, $password)
	{
		$realUser = $this->dao->read($username);
		//print_r ($realUser);
		$guessHash = self::hash_password($password);
		//print_r ($guessHash);
		
		return ($guessHash === $realUser->hash && $username === $realUser->username);
	}
	
	public static function hash_password($password)
	{
		return crypt($password, '$2a$' . '10$somesaltafawefwfesfwrfwfd' . '$');
	}
}

?>